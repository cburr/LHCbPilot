#!/bin/bash
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

PILOTBRANCH=pilot_loggers
LHCBPILOTBRANCH=master
DEBUG=True

#DIRAC setup
DIRACSETUP=LHCb-Certification
CSURL=dips://lbvobox18.cern.ch:9135/Configuration/Server
DIRACSE=CERN-SWTEST

#DIRACUSERDN=/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=fstagni/CN=693025/CN=Federico Stagni
DIRACUSERDN=/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=wkrzemie/CN=643820/CN=Wojciech Jan Krzemien
DIRACUSERROLE=lhcb_prmgr


echo -e "**** Starting LHCbPilot tests with PilotLogger ****\n"

#FIXME THIS IS NOT NEEDED!!!

echo -e '***' $(date -u) "**** Getting the tests ****\n"

mkdir -p $PWD/TestCode
cd $PWD/TestCode

git clone https://github.com/wkrzemien/Pilot.git
cd Pilot
git checkout $PILOTBRANCH
cd ..

git clone ssh://git@gitlab.cern.ch:7999/wkrzemie/LHCbPilot.git
cd LHCbPilot
git checkout $LHCBPILOTBRANCH

echo `pwd`
cd ../..
echo `pwd`

echo -e '***' $(date -u) "**** Got the tests ****\n"

source TestCode/Pilot/tests/CI/pilot_ci.sh
#<---- this file contains the tests logic

echo -e '***' $(date -u) "**** Pilot INSTALLATION START ****\n"

prepareForPilot
preparePythonEnvironment
cd $PILOTINSTALLDIR
echo '==> [LHCbPilotLogger]'
RabbitServerCleanup #to assure that the queue is empty
python Test_LHCbPilotLogger.py
RabbitServerCleanup #to assure that the queue is empty
cd ../
